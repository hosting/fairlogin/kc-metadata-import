FROM python:3.10.13-slim-bookworm
RUN mkdir -p /opt/importer
WORKDIR /opt/importer
COPY LICENSE .
COPY analysis.py .
COPY main.py .
COPY database.py .
COPY entrypoint.sh .
COPY exclude.json .
COPY requirements.txt .
COPY xml_namespaces.json .
COPY attribute_mapping.json .
COPY mds-v2.cer .

RUN pip install -r requirements.txt

ENTRYPOINT ["/opt/importer/entrypoint.sh"]
