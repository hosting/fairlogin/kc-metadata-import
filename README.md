# Keycloak SAML 2.0 Metadata importer

Reads a [SAML 2.0 Metadata](https://wiki.oasis-open.org/security#SAML_V2.0_Standard) XML document and creates or updates Identity Providers in [Keycloak](https://www.keycloak.org/).

Works perfectly in combination with the [Keycloak home IdP discovery plugin](https://github.com/sventorben/keycloak-home-idp-discovery), which offers to enter e-mail and then redirects to the matching IdP for login.

## Requirements
* Python3.10+
* [python-keycloak](https://pypi.org/project/python-keycloak/)
* [pyXMLSecurity](https://github.com/IdentityPython/pyXMLSecurity) for XML Signature verification
* (optional) an external database, to keep track of the syncs, if there is no external DB, sqlite has to be selected 

## Use
Install the required Python packages:
```sh
pip install -r requirements.txt
```
Prepare the configuration as mentioned below.

Run the sync script with
```sh
./main.py
```

You may want to run the script once a day to update the IdPs, e.g. via cron or systemd-timers.

## Configuration
### Environment variables
 - `SAML2_METADATA_URL` (URL to SAML 2.0 Metadata document)
 - `SAML2_METADATA_CRT_FILE` (path to the X.509 certificate to use for XML Signature verification of the Metadata)
 - `SAML2_METADATA_INSECURE` (if no XML Signature verification and no `validUntil` expiration check should be performed; NOT RECOMMENDED)
 - `SAML2_METADATA_EXCLUDE_JSON_PATH` (path ending with /, default is ./)
 - `KEYCLOAK_URL` (URL to Keycloak instance)
 - `KEYCLOAK_REALM` (Keycloak realm)
 - `KEYCLOAK_USER` (Keycloak user)
 - `KEYCLOAK_PASSWORD` (Keycloak users password)
 - `DB_TYPE` (`postresql`, `mysql` or `sqlite`)

> Note that *either* `SAML2_METADATA_CRT_FILE` must be set (to enable XML Signature verification of signed SAML 2.0 Metadata) *or* `SAML2_METADATA_INSECURE` (to skip it). If both are set `SAML2_METADATA_INSECURE` is ignored.

For external databases:
 - `DB_USER` (database user)
 - `DB_PASSWORD` (database password)
 - `DB_HOST` (dbms host)
 - `DB_PORT` (dbms port)
 - `DB_DATABASE` (database)

For creation of the xml need to publish a servie provider:
 - `KEYCLOAK_FRONTENDURL`
 - `SAML2_EXPORT_TEMPLATE` xml file as template
 - `SAML2_EXPORT_OUTPUT` xml file generated

### Mappings
Edit `attribute_mapping.json`, key is the SAML2 attribute, value is the Keycloak user attribute.

### Exclude IDPs and/or SPs
Edit `exclude.json`. For SPs list the `clientId`, for IdPs the `entityID`.

### Analyse XML data
In order to judge, if certain attributes are suitable for mappings, you may run a script, which creates a csv file for further analysis.      
```sh
./analysis.py
```
