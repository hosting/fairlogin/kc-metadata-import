#!/usr/bin/env python3
# Keycloak SAML2 Metadata Importer
# Copyright (C) 2023 Armin Felder
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# parse an IdP XML feed and export as csv for further analysis which attributes fit best for discovery

from lxml import etree

import ecs_logging
import json
import logging
import os
import requests as requests
import sys


logger = logging.getLogger("app")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(ecs_logging.StdlibFormatter())
logger.addHandler(handler)


with open("./xml_namespaces.json") as f:
    NS = json.load(f)


def get_metadata():
    url = os.getenv('SAML2_METADATA_URL')
    if not url:
        sys.exit("Environment variable SAML2_METADATA_URL not set.")
    r = requests.get(url, allow_redirects=True)
    parser = etree.XMLParser(resolve_entities=False)
    root = etree.fromstring(r.content, parser)
    return root


def get_entity_descriptors():
    root = get_metadata()
    entity_descriptors = root.xpath("//md:EntityDescriptor", namespaces=NS)

    return entity_descriptors


descriptors = get_entity_descriptors()

idps = list()

for descriptor in descriptors:
    idp_search = descriptor.xpath(".//md:IDPSSODescriptor", namespaces=NS)

    idp = dict()

    idp["entity_id"] = (descriptor.attrib["entityID"])

    if len(idp_search):

        names = idp_search[0].xpath("./md:Extensions/mdui:UIInfo/mdui:DisplayName[@xml:lang='en']", namespaces=NS)

        idp["institution"] = ""
        if len(names):
            idp["institution"] = names[0].text

        disco_hints = idp_search[0].xpath("./md:Extensions/mdui:DiscoHints/mdui:DomainHint", namespaces=NS)

        domains = []

        for disco_hint in disco_hints:
            domains.append(disco_hint.text)

        idp["disco_hints"] = domains

        shib_scope = idp_search[0].xpath("./md:Extensions/shibmd:Scope", namespaces=NS)

        idp["shib_scope"] = ""
        if len(shib_scope):
            idp["shib_scope"] = shib_scope[0].text

        idps.append(idp)


with open("report.csv", "a") as f:
    f.write("{0},{1},{2},{3}\n".format("entity_id", "institution", "shib_scope", "domain_hint"))
    for idp in idps:

        f.write("{0},{1},{2},{3}\n".format(idp["entity_id"], idp["institution"], idp["shib_scope"], ";".join(idp["disco_hints"])))
