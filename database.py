# Keycloak SAML2 Metadata Importer
# Copyright (C) 2023 Armin Felder
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from peewee import *
import ecs_logging
import logging

logger = logging.getLogger("database")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(ecs_logging.StdlibFormatter())
logger.addHandler(handler)


db_type = None
db_user = None
db_password = None
db_host = None
db_port = None
db_database = None

db = DatabaseProxy()


class BaseModel(Model):
    class Meta:
        database = db


class Synced_sp(BaseModel):
    id = IntegerField(primary_key=True)
    name = CharField(unique=True, null=False, index=True)
    config_hash = CharField()
    created = IntegerField()
    last_updated = IntegerField()


class Synced_idp(BaseModel):
    id = IntegerField(primary_key=True)
    name = CharField(unique=True, null=False, index=True)
    config_hash = CharField()
    created = IntegerField()
    last_updated = IntegerField()


def init():
    global db
    db_impl = None
    match db_type:
        case "POSTGRESQL":
            db_impl = PostgresqlDatabase(host=db_host,
                                         port=db_port,
                                         database=db_database,
                                         user=db_user,
                                         password=db_password)
        case "MYSQL":
            db_impl = MySQLDatabase(host=db_host,
                                    port=db_port,
                                    database=db_database,
                                    user=db_user,
                                    password=db_password)
        case "SQLITE":
            db_impl = SqliteDatabase('./data/sync.db' if db_database is None else db_database)
        case _:
            logger.fatal("{0} is invalid DB_TYPE".format(db_type))
            exit(1)

    db.initialize(db_impl)

    db.connect()
    db.create_tables([Synced_sp, Synced_idp])
