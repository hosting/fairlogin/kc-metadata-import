#!/bin/bash

export SAML2_METADATA_URL=${SAML2_METADATA_URL:-https://mds.edugain.org/edugain-v2.xml}
if test -n "$SAML2_METADATA_INSECURE"; then
	export SAML2_METADATA_INSECURE
else
	export SAML2_METADATA_CRT_FILE=${SAML2_METADATA_CRT_FILE:-./mds-v2.cer}
fi
export KEYCLOAK_URL=${KEYCLOAK_URL:-http://keycloak:8080/auth/}
export KEYCLOAK_REALM=${KEYCLOAK_REALM:-sso}
#export KEYCLOAK_USER=
#export KEYCLOAK_PASSWORD=
#export KEYCLOAK_FRONTENDURL=
#export SAML2_EXPORT_TEMPLATE=
#export SAML2_EXPORT_OUTPUT=
export DB_TYPE=sqlite
export DB_DATABASE="/persistent/syncdb.sqlite"

python ./main.py
