#!/usr/bin/env python3
# Keycloak SAML2 Metadata Importer
# Copyright (C) 2023 Armin Felder
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import hashlib
import json
import os
import io
import sqlite3
import sys
import xmlsec
from datetime import datetime, timezone

import database

import keycloak.exceptions
from lxml import etree

import ecs_logging
import logging
import requests as requests
from keycloak import KeycloakAdmin,KeycloakGetError
from keycloak.exceptions import raise_error_from_response

URL_CERTS = "realms/{realm-name}/protocol/openid-connect/certs"

logger = logging.getLogger("app")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(ecs_logging.StdlibFormatter())
logger.addHandler(handler)


class Metadata_importer():

    def __init__(self):

        self.__get_settings()
#        self.__db = self.__get_sync_db()
        self.__init_db()
        self.__get_mappings()
        self.__get_xmlns()
        self.__get_excluded()

    def __get_settings(self):
        self.metadata_exclude_json_path = os.getenv('SAML2_METADATA_EXCLUDE_JSON_PATH','./')
        self.metadata_feed_url = os.getenv('SAML2_METADATA_URL')
        self.metadata_feed_crt = os.getenv('SAML2_METADATA_CRT_FILE')
        self.metadata_feed_insecure = os.getenv('SAML2_METADATA_INSECURE', False)
        if self.metadata_feed_crt and self.metadata_feed_insecure:
            logger.warning("Ignoring SAML2_METADATA_INSECURE because SAML2_METADATA_CRT_FILE is set.")
            self.metadata_feed_insecure = False
        self.keycloak_url = os.getenv('KEYCLOAK_URL')
        self.keycloak_realm = os.getenv('KEYCLOAK_REALM')
        self.keycloak_user = os.getenv('KEYCLOAK_USER')
        self.keycloak_password = os.getenv('KEYCLOAK_PASSWORD')
        self.keycloak_frontendurl=os.getenv('KEYCLOAK_FRONTENDURL',False)
        self.db_type = os.getenv('DB_TYPE').upper()
        self.db_user = os.getenv('DB_USER')
        self.db_password = os.getenv('DB_PASSWORD')
        self.db_host = os.getenv("DB_HOST")
        self.db_port = os.getenv('DB_PORT')
        self.db_database = os.getenv('DB_DATABASE')
        self.saml2_export_template=os.getenv('SAML2_EXPORT_TEMPLATE',False)
        self.saml2_export_output=os.getenv('SAML2_EXPORT_OUTPUT',False)
        if (self.saml2_export_template and not self.saml2_export_output) or (not self.saml2_export_template and self.saml2_export_output):
           logger.warning("SAML2_EXPORT_TEMPLATE and SAML2_EXPORT_OUTPUT have to be specified.")

    def __get_mappings(self):
        with open("./attribute_mapping.json") as f:
            self.mappings = json.load(f)

    def __get_xmlns(self):
        with open("./xml_namespaces.json") as f:
            self.NS = json.load(f)

    def __get_excluded(self):
        with open(self.metadata_exclude_json_path+"exclude.json") as f:
            self.excluded = json.load(f)

    def __get_sync_db(self):
        return sqlite3.connect('./db.db')

    def __init_db(self):

        database.db_type = self.db_type
        database.db_user = self.db_user
        database.db_password = self.db_password
        database.db_host = self.db_host
        database.db_port = self.db_port
        database.db_database = self.db_database

        database.init()

    def __add_sp_to_db(self, name, config_hash, sync_date):
        sp = database.Synced_sp(name=name, config_hash=config_hash, created=sync_date, last_updated=sync_date)
        sp.save()

    def __add_idp_to_db(self, name, config_hash, sync_date):
        idp = database.Synced_idp(name=name, config_hash=config_hash, created=sync_date, last_updated=sync_date)
        idp.save()

    def __update_sp_record(self, name, config_hash, sync_date):
        rows = database.Synced_sp.update(config_hash=config_hash, last_updated=sync_date).where(database.Synced_sp.name == name).execute()

        if not rows:
            self.__add_sp_to_db(name=name, config_hash=config_hash, sync_date=sync_date)

    def __update_idp_record(self, name, config_hash, sync_date):
        rows = database.Synced_idp.update(config_hash=config_hash, last_updated=sync_date).where(database.Synced_idp.name == name).execute()

        if not rows:
            self.__add_idp_to_db(name, config_hash, sync_date)

    def __get_sp_record(self, name):
        records = database.Synced_sp.select(database.Synced_sp.id, database.Synced_sp.name, database.Synced_sp.config_hash).where(database.Synced_sp.name == name)
        result = None
        if len(records):
            for row in records:
                result = {
                    "id": int(row.id),
                    "name": str(row.name),
                    "config_hash": str(row.config_hash),
                }
        return result

    def __get_idp_record(self, name):
        records = database.Synced_idp.select(database.Synced_idp.id, database.Synced_idp.name, database.Synced_idp.config_hash).where(database.Synced_idp.name == name)
        result = None
        if len(records):
            for row in records:
                result = {
                    "id": int(row.id),
                    "name": str(row.name),
                    "config_hash": str(row.config_hash),
                }
        return result

    def validate_metadata_signature(self, root):
        certspec = self.metadata_feed_crt
        try:
            xmlsec.verified(root, certspec)
        except xmlsec.exceptions.XMLSigException:
            msg="XML Signature verification failed"
            logger.error(msg)
            sys.exit(msg)

    def parse_date(self, date):
        try:
            return datetime.fromisoformat(date)
        except ValueError:
            return datetime.strptime(date, "%Y-%m-%dT%H:%M:%S%z")

    def check_expiration(self, date):
        if self.metadata_feed_insecure:
            logger.warning("Skipping validUntil check")
            return
        if not date:
            logger.warning("Metadata has no 'validUntil'")
            return
        now = datetime.now(timezone.utc)
        valid_until = self.parse_date(date)
        delta = now - valid_until
        if delta.days > 0:
            msg="Metadata expired at {} (now is {})".format(valid_until, now)
            logger.error(msg)
            sys.exit(msg)

    def check_metadata(self, root):
        self.check_expiration(root.get("validUntil"))
        signed = root.xpath("/md:EntitiesDescriptor/ds:Signature", namespaces={"md": "urn:oasis:names:tc:SAML:2.0:metadata", "ds": "http://www.w3.org/2000/09/xmldsig#"})
        if len(signed) and self.metadata_feed_crt:
            self.validate_metadata_signature(root)
        elif len(signed) and not self.metadata_feed_crt and not self.metadata_feed_insecure:
            msg="Set SAML2_METADATA_CRT_FILE to enable XML Signature verification (strongly recommended) or SAML2_METADATA_INSECURE to skip it."
            logger.error(msg)
            sys.exit(msg)
        else:
            logger.warning("Skipping XML Signature verification")
        return root

    def get_metadata(self):
        requests.packages.urllib3.util.connection.HAS_IPV6 = False
        url = self.metadata_feed_url
        header = {"user-agent": "keycloak-metadata-importer"}

        r = requests.get(url, allow_redirects=True, headers=header)
        r.raise_for_status()
        parser = etree.XMLParser(resolve_entities=False)
        root = etree.fromstring(r.content, parser)
        return self.check_metadata(root)

    def get_entity_descriptors(self):
        root = self.get_metadata()
        entity_descriptors = root.xpath("//md:EntityDescriptor", namespaces=self.NS)

        return entity_descriptors

    def split_into_SP_and_IDP(self):
        entity_descriptors = self.get_entity_descriptors()

        sps = list()
        idps = list()

        for entity_descriptor in entity_descriptors:
            idp_search = entity_descriptor.xpath(".//md:IDPSSODescriptor", namespaces=self.NS)
            sp_search = entity_descriptor.xpath(".//md:SPSSODescriptor", namespaces=self.NS)
            if len(idp_search):
                idps.append(entity_descriptor)
            elif len(sp_search):
                sps.append(entity_descriptor)

        return {"idps": idps, "sps": sps}

    def postprocess_client_obj(self, sp, obj):
        signAuthN = sp.get("AuthnRequestsSigned")
        if signAuthN is None or signAuthN != "true":
            if "attributes" not in obj:
                obj["attributes"] = dict()
            obj["attributes"]["saml.client.signature"] = "false"

        for mapper in obj["protocolMappers"]:
            name = mapper["name"]
            if name in self.mappings:
                mapper["config"]["user.attribute"] = self.mappings[name]
            else:
                logger.warning("attribute not mapped", extra={"attribute": name, "client_id": obj["clientId"]})
        return obj

    def __get_config_hash(self, config: dict):

        config_json = json.dumps(config, sort_keys=True)
        m = hashlib.sha256()
        m.update(config_json.encode())
        config_hash = m.hexdigest()
        return config_hash

    def __get_utc_timestamp(self):
        date = datetime.utcnow()
        utc_time = int(datetime.timestamp(date))
        return utc_time

    def kc_import_sp(self, sps):

        keycloak_admin = KeycloakAdmin(server_url=self.keycloak_url,
                                       username=self.keycloak_user,
                                       password=self.keycloak_password,
                                       realm_name=self.keycloak_realm,
                                       verify=True)

        for sp in sps:
            obj = self.convert_client_xml(etree.tostring(sp, xml_declaration=True, encoding='UTF-8', pretty_print=True).decode("utf-8"), keycloak_admin.token['access_token'])
            if "error" in obj:
                logger.warning("failed", extra={"error": obj["error"]})
                continue
            obj = self.postprocess_client_obj(sp, obj)
            displayName_nodes = sp.xpath(".//md:Extensions/mdui:UIInfo/mdui:DisplayName[@xml:lang='en']", namespaces=self.NS)
            description_nodes = sp.xpath(".//md:Extensions/mdui:UIInfo/mdui:Description[@xml:lang='en']", namespaces=self.NS)
            # privacynote_nodes = sp.xpath(".//md:Extensions/mdui:UIInfo/mdui:PrivacyStatementURL[@xml:lang='en']", namespaces=self.NS)
            logo_nodes = sp.xpath(".//md:Extensions/mdui:UIInfo/mdui:Logo", namespaces=self.NS)

            if len(description_nodes):
                obj["description"] = description_nodes[0].text[:255]
            if len(displayName_nodes):
                obj["name"] = displayName_nodes[0].text[:255]
            if len(logo_nodes):
                obj["attributes"]["logoUri"] = logo_nodes[0].text

            if obj["clientId"] not in self.excluded["sps"]:

                new_hash = self.__get_config_hash(obj)
                record = self.__get_sp_record(obj["clientId"])

                # SP is unknown to sync database
                if record is None:
                    # create new SP in keycloak
                    try:
                        keycloak_admin.create_client(obj)
                        logger.info("SP created", extra={"clientId": obj["clientId"]})
                        self.__add_sp_to_db(obj["alias"], new_hash, self.__get_utc_timestamp())

                    except:
                        # update existing SP in keycloak
                        logger.info("SP update", extra={"clientId": obj["clientId"]})
                        result = keycloak_admin.get_client_id(obj["clientId"])
                        try:
                            keycloak_admin.update_client(result, obj)
                            self.__add_sp_to_db(obj["clientId"], new_hash, self.__get_utc_timestamp())
                        except keycloak.exceptions.KeycloakPutError as e:
                            print(e)
                # SP is known to sync database
                elif record["config_hash"] != new_hash:
                    try:
                        # update existing SP in keycloak
                        logger.info("SP update", extra={"clientId": obj["clientId"]})
                        result = keycloak_admin.get_client_id(obj["clientId"])

                        keycloak_admin.update_client(result, obj)
                        self.__update_sp_record(obj["clientId"], new_hash, self.__get_utc_timestamp())
                    except keycloak.exceptions.KeycloakPutError as e:
                        print(e)

    def kc_import_idp(self, idps):

        POST_LOGIN_URL_TEMPLATE="%(keycloak_frontendurl)s/realms/%(keycloak_realm)s/broker/%(alias)s/endpoint"
        post_login_urls=[]

        keycloak_admin = KeycloakAdmin(server_url=self.keycloak_url,
                                       username=self.keycloak_user,
                                       password=self.keycloak_password,
                                       realm_name=self.keycloak_realm,
                                       verify=True)

        for idp in idps:
            hints = set()

            xmlstr = etree.tostring(idp, encoding='utf8', method='xml').decode("utf8")

            discoHints = idp.xpath(".//mdui:DomainHint", namespaces=self.NS)
            scopes = idp.xpath(".//shibmd:Scope", namespaces=self.NS)

            displayNames = idp.xpath(".//md:Extensions/mdui:UIInfo/mdui:DisplayName[@xml:lang='en']",
                                     namespaces=self.NS)
            displayName = ""
            if len(displayNames) > 0:
                displayName = displayNames[0].text
            else:
                OrgDisplayNames = idp.xpath(".//md:OrganizationDisplayName[@xml:lang='en']", namespaces=self.NS)
                if len(OrgDisplayNames) > 0:
                    displayName = OrgDisplayNames[0].text

            for scope in scopes:
                hints.add(scope.text)

            for hint in discoHints:
                hints.add(hint.text)

            idp_obj = dict()
            alias = idp.get("entityID")
            alias = alias.replace("https://", "")
            alias = alias.replace("http://", "")
            alias = alias.replace("/", "_")
            alias = alias.replace(".", "_")

            idp_obj["providerId"] = "saml"
            idp_obj["alias"] = alias
            idp_obj["displayName"] = displayName

            idp_obj["config"] = self.convert_idp_xml(xmlstr, keycloak_admin.token['access_token'], alias)
            idp_obj["config"]["nameIDPolicyFormat"] = "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
            idp_obj["config"]["home.idp.discovery.domains"] = "##".join(hints)

            if "signingCertificate" in idp_obj["config"] and idp_obj["config"]["signingCertificate"] != "":
                idp_obj["config"]["validateSignature"] = True

            if idp.get("entityID") not in self.excluded["idps"]:

                new_hash = self.__get_config_hash(idp_obj)
                record = self.__get_idp_record(idp_obj["alias"])

                # IDP is unknown to sync database
                if record is None:
                    # create new IDP in keycloak
                    try:
                        keycloak_admin.create_idp(idp_obj)
                        logger.info("IDP created", extra={"alias": idp_obj["alias"]})
                        self.__add_idp_to_db(idp_obj["alias"], new_hash, self.__get_utc_timestamp())
                        self.__add_idp_mapping(idp_obj["alias"])
                    except:
                        # update existing SP in keycloak
                        logger.info("IDP update", extra={"alias": idp_obj["alias"]})
                        try:
                            logger.info("update existing IDP", extra={"alias": alias})
                            keycloak_admin.update_idp(alias, idp_obj)
                            self.__add_idp_to_db(idp_obj["alias"], new_hash, self.__get_utc_timestamp())
                            self.__add_idp_mapping(idp_obj["alias"])
                        except keycloak.exceptions.KeycloakPutError as e:
                            print(e)
                # IDP is known to sync database
                elif record["config_hash"] != new_hash:
                    try:
                        # update existing IDP in keycloak
                        logger.info("IDP update", extra={"alias": idp_obj["alias"]})
                        keycloak_admin.update_idp(alias, idp_obj)
                        self.__update_idp_record(idp_obj["alias"], new_hash, self.__get_utc_timestamp())
                        self.__add_idp_mapping(idp_obj["alias"])
                    except keycloak.exceptions.KeycloakPutError as e:
                        print(e)

                post_login_urls+=[
                  POST_LOGIN_URL_TEMPLATE % {"keycloak_frontendurl":self.keycloak_frontendurl if self.keycloak_frontendurl else self.keycloak_url,
                      "keycloak_realm":self.keycloak_realm,"alias":idp_obj["alias"]}
                ]

        return post_login_urls

    def __add_idp_mapping(self, idp_alias):
        keycloak_admin = KeycloakAdmin(server_url=self.keycloak_url,
                                       username=self.keycloak_user,
                                       password=self.keycloak_password,
                                       realm_name=self.keycloak_realm,
                                       verify=True)

        mappers = keycloak_admin.get_idp_mappers(idp_alias)

        for key, value in self.mappings.items():
            mapper_config = {
                "name": value,
                "identityProviderMapper": "saml-user-attribute-idp-mapper",
                "identityProviderAlias": idp_alias,
                "config": {
                    "name": value,
                    "user.attribute": value,
                    "attribute.name": key,
                    "syncMode": "INHERIT"
                }
            }

            update = False
            mapper_id = ""
            for mapper in mappers:
                if mapper["name"] == value:
                    update = True
                    mapper_id = mapper["id"]
            if update:
                mapper_config["id"] = mapper_id
                keycloak_admin.update_mapper_in_idp(idp_alias, mapper_id, mapper_config)
            else:
                keycloak_admin.add_mapper_to_idp(idp_alias, mapper_config)

    def convert_idp_xml(self, xml: str, token: str, alias: str):
        url = "{0}/admin/realms/{1}/identity-provider/import-config".format(self.keycloak_url, self.keycloak_realm)
        authString = "Bearer {0}".format(token)
        result = requests.post(url, allow_redirects=True, files={"file": xml}, data={"providerId": "saml"}, headers={"Authorization": authString})
        obj = result.json()

        return obj

    def convert_client_xml(self, xml: str, token: str):
        url = "{0}/admin/realms/{1}/client-description-converter".format(self.keycloak_url if not self.keycloak_url.endswith("/") else self.keycloak_url[:-1], self.keycloak_realm)
        authString = "Bearer " + token
        result = requests.post(url, allow_redirects=True, data=xml.encode('utf-8'), headers={"Authorization": authString, "Content-Type": "application/json; charset=utf-8"})
        obj = result.json()

        return obj


    def generate_metadata_output(self,broker_post_login_redirect_url):
        LINE_TEMPLATE='<md:AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="%(url)s" index="%(idx)d"/>'
        keycloak_admin = KeycloakAdmin(server_url=self.keycloak_url,
                                       username=self.keycloak_user,
                                       password=self.keycloak_password,
                                       realm_name=self.keycloak_realm,
                                       verify=True)
        params_path = {"realm-name": self.keycloak_realm}
        data_raw = keycloak_admin.connection.raw_get(URL_CERTS.format(**params_path))
        jdata=raise_error_from_response(data_raw, KeycloakGetError)
        keys=jdata["keys"]
        key_to_use=None
        for k in keys:
            if k['use']=='sig':
                key_to_use=k["x5c"][0]
                break
        if not key_to_use:
           raise Exception("no key found")
        
        chunked_key=[]
        for i in range(0,len(key_to_use),64):
            chunked_key.append(key_to_use[i:i+64])
        
        formated_key="\n".join(chunked_key)
        formated_urls=[]
        for (i,line) in enumerate(broker_post_login_redirect_url):
            formated_urls.append( LINE_TEMPLATE % {"idx":i+1,"url":line})
        formated_urls_str="\n".join(formated_urls)
        update_time=datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        template=io.open(self.saml2_export_template,mode="r",encoding="utf-8").read()
        result=template.replace("$REGISTRATION-INSTANT$",update_time)
        result=result.replace("$IDP-CALLBACKS$",formated_urls_str)
        result=result.replace("$CERT-ENCRYPTION$",formated_key)
        with io.open(self.saml2_export_output,mode="w",encoding="utf-8") as outfile:
            outfile.write(result)


    def sync(self):
        lists = self.split_into_SP_and_IDP()

        self.kc_import_sp(lists["sps"])
        broker_post_login_redirect_urls=self.kc_import_idp(lists["idps"])
        if self.saml2_export_template and self.saml2_export_output:
           self.generate_metadata_output(broker_post_login_redirect_urls)

if __name__ == "__main__":
    logger.info("starting")

    metadata_importer = Metadata_importer()

    metadata_importer.sync()

    logger.info("finished")
